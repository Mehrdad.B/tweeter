package cs.sbu.tweeter;

import java.util.ArrayList;
import java.util.Random;

public class Tweets {
    private String tweet ;
    private String tweetid ;
    private ArrayList<TweeterUser> likes;
    public Tweets(String tweet , int num)
    {
        this.tweet=tweet;
        likes = new ArrayList<>();
        setid(num);
    }
    public void setid(int num)
    {
        this.tweetid = Integer.toString(num) + "_" + generateid();
    }
    public String generateid()
    {
        Random id = new Random();
        int Id = id.nextInt(250);
        return Integer.toString(Id);
    }
    public void addlike(TweeterUser user)
    {
        likes.add(user);
    }
    public String getId()
    {
        return tweetid;
    }
    public String getlikeuser(int ind)
    {
        return likes.get(ind).getname();
    }
    public String getTweet()
    {
        return tweet;
    }
    public int likenumber()
    {
        return likes.size();
    }

}

