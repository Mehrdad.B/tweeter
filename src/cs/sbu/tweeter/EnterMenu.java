package cs.sbu.tweeter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Scanner;

public class EnterMenu {
    private static int index ;
    private static int number = 0;
    private  static  ArrayList<TweeterUser> alluser = new ArrayList<>();
    protected static TweeterUser user;
    public static void main(String[] arg)
    {
        menu();
    }
    public static void menu()
    {
        Scanner in = new Scanner(System.in);
        System.out.println("1 - Sign Up ");
        System.out.println("2 - Log In ");
        System.out.println("3 - Quit ");
        System.out.println("Enter your desire action ");
        String action = in.next();
        if(action.equals("1"))
        {
            System.out.print("Enter the name :  ");
            String name = in.next();
            while(isnameexisted(name))
            {
                System.out.println("Username existed! Enter another one.");
                name = in.next();
            }
            System.out.print("Enter the password :  ");
            String pass = in.next();
            user = new TweeterUser(name , pass);
            alluser.add(user);
            System.out.println("-------------------------");
            System.out.println("Enter C to continue ");
            String enter = in.next();
            while (!enter.equals("C"))
            {
                System.out.println("Bad input! try again. ");
                enter = in.next();
            }
            if(enter.equals("C"))
            {
                index=findindex(name);
                actions();
            }
        }
        if(action.equals("2"))
        {
            boolean flag = false;
            System.out.print("Enter the name :  ");
            String name = in.next();
            while(!isnameexisted(name))
            {
                System.out.println("Username not found! try again.");
                name = in.next();
            }
            System.out.print("Enter the password :  ");
            String pass = in.next();
            while(!ispassexisted(pass))
            {
                System.out.println("password is incorrect! try again.");
                pass = in.next();
            }
            System.out.println("Log in successfully ");
            System.out.println("-------------------------");
            System.out.println("Enter C to continue ");
            String enter = in.next();
            while (!enter.equals("C"))
            {
                System.out.println("Bad input! try again. ");
                enter = in.next();
            }
            if(enter.equals("C"))
            {
                index=findindex(name);
                actions();
            }
        }
        if(action.equals("3"))
        {
            System.exit(1);
        }

    }
    public static void cls()
    {
        for (int i=0 ; i<10 ; i++)
        {
            System.out.println();
        }
    }
    public static boolean isnameexisted(String name)
    {
        for(int i=0 ; i<alluser.size() ; i++)
        {
            if(name.equals(alluser.get(i).getname()))
                return true;
        }
        return false;
    }
    public static boolean ispassexisted(String pass)
    {
        for(int i=0 ; i<alluser.size() ; i++)
        {
            if(pass.equals(alluser.get(i).getpass()))
                return true;
        }
        return false;
    }
    public static int findindex(String username)
    {
        for(int i=0 ; i<alluser.size() ; i++)
        {
            if(username.equals(alluser.get(i).getname()))
            {
                return i;
            }
        }
        return 0;
    }
    public static void actions()
    {
        Scanner in = new Scanner(System.in);
        System.out.println("1  - My profile ");
        System.out.println("2  - Tweet ");
        System.out.println("3  - Follow ");
        System.out.println("4  - Unfollow ");
        System.out.println("5  - Followers ");
        System.out.println("6  - Following ");
        System.out.println("7  - Timeline ");
        System.out.println("8  - Profile ");
        System.out.println("9  - Like ");
        System.out.println("10 - Log out ");
        System.out.println("Enter your desire action ");
        String action = in.next();
        if(action.equals("1"))
        {
            System.out.println("---------------------");
            System.out.println("User name is : " + alluser.get(index).getname());
            for(int i=0 ; i<alluser.get(index).tweetsize() ; i++)
            {
                System.out.println("---------------------");
                System.out.println(Myprofile(i));
            }
            System.out.println("---------------------");
            System.out.println("Press C to continue");
            String ctn = in.next();
            while(!ctn.equals("C"))
            {
                System.out.println("Bad input! try again. ");
                ctn = in.next();
            }
            cls();
            actions();
        }
        else if(action.equals("2"))
        {
            Tweet();
        }
        else if(action.equals("3"))
        {
            Follow();
        }
        else if(action.equals("4"))
        {
            Unfollow();
        }
        else if(action.equals("5"))
        {
            for(int i=0 ; i<alluser.get(index).followerssize() ; i++)
            {
                System.out.println("----------------------------");
                System.out.println(Followers(i));
            }
            System.out.println("----------------------------");
            System.out.println("Press C to continue");
            String ctn = in.next();
            while(!ctn.equals("C"))
            {
                System.out.println("Bad input! try again. ");
                ctn = in.next();
            }
            cls();
            actions();
        }
        else if(action.equals("6"))
        {
            for(int i=0 ; i<alluser.get(index).followingsize() ; i++)
            {
                System.out.println("----------------------------");
                System.out.println(Following(i));
            }
            System.out.println("----------------------------");
            System.out.println("Press C to continue");
            String ctn = in.next();
            while(!ctn.equals("C"))
            {
                System.out.println("Bad input! try again. ");
                ctn = in.next();
            }
            cls();
            actions();
        }
        else if(action.equals("7"))
        {
            ArrayList<String> sortedtweets = Timeline();
            for(int i=0 ; i<sortedtweets.size() ; i++)
            {
                System.out.println("----------------------------");
                System.out.println(sortedtweets.get(i));
            }
            System.out.println("----------------------------");
            System.out.println("Press C to continue");
            String ctn = in.next();
            while(!ctn.equals("C"))
            {
                System.out.println("Bad input! try again. ");
                ctn = in.next();
            }
            cls();
            actions();
        }
        else if(action.equals("8"))
        {
            ArrayList<String> usertweets = Profile();
            for (int i=0 ; i<usertweets.size() ; i++)
            {
                System.out.println("----------------------------");
                System.out.println(usertweets.get(i));
            }
            System.out.println("----------------------------");
            System.out.println("Press C to continue");
            String ctn = in.next();
            while(!ctn.equals("C"))
            {
                System.out.println("Bad input! try again. ");
                ctn = in.next();
            }
            cls();
            actions();
        }
        else if(action.equals("9"))
        {
            liketweet();
        }
        else if(action.equals("10"))
        {
            cls();
            menu();
        }
        else {
            cls();
            actions();
        }
    }
    public static String Myprofile(int ind)
    {
        String inf = "tweet id is : " + alluser.get(index).getid(ind) +
                " --- " + "tweet text : " + alluser.get(index).gettweet(ind) +
                " --- " + alluser.get(index).likenum(ind) + " likes" ;
        return inf;
    }
    public static void Tweet()
    {
        Scanner in = new Scanner(System.in);
        System.out.println("---------------------");
        System.out.println("Write a tweet : ");
        String tweet = in.nextLine() ;
        while(tweet.length()>=140)
        {
            System.out.println("Tweet most have less than 140 character. ");
            tweet = in.nextLine();
        }
        number++ ;
        Tweets txt = new Tweets(tweet , number);
        alluser.get(index).addtweet(txt);
        System.out.println("---------------------");
        System.out.println("Press C to continue");
        String ctn = in.next();
        while(!ctn.equals("C"))
        {
            System.out.println("Bad input! try again. ");
            ctn = in.next();
        }
        cls();
        actions();
    }
    public static void Follow()
    {
        Scanner in = new Scanner(System.in);
        boolean flag = false;
        System.out.println("------------------------------");
        System.out.println("Enter a username to follow : ");
        String username = in.next() ;
        if(!isfollowed(username))
        {
            for(int i=0 ; i<alluser.size() ; i++)
            {
                if(username.equals(alluser.get(i).getname()))
                {
                    alluser.get(i).addfollowers(alluser.get(index));
                    alluser.get(index).addfollowing(alluser.get(i));
                    flag = true;
                }
            }
        }
        else if(isfollowed(username))
        {
            System.out.println("User have been followed!");
            Follow();
        }
        if(!flag)
        {
            System.out.println("Username not found!");
            Follow();
        }
        else
        {
            cls();
            actions();
        }
    }
    public static boolean isfollowed(String name)
    {
        for(int i=0 ; i<alluser.get(index).followingsize() ; i++)
        {
            if(name.equals(alluser.get(index).getfollowing(i).getname()))
                return true;
        }
        return false;
    }
    public static void Unfollow()
    {
        Scanner in = new Scanner(System.in);
        boolean flag = false;
        System.out.println("--------------------------------");
        System.out.println("Enter a username to unfollow : ");
        String username = in.next() ;
        for(int i=0 ; i<alluser.size() ; i++)
        {
            if(username.equals(alluser.get(i).getname()))
            {
                alluser.get(i).deletefollowers(alluser.get(index));
                alluser.get(index).deletefollowing(alluser.get(i));
                flag = true;
            }
        }
        if(!flag)
        {
            System.out.println("Username not found!");
            Follow();
        }
        else
        {
            cls();
            actions();
        }
    }
    public static String Followers(int ind)
    {
        return alluser.get(index).getfollowersname(ind);
    }
    public static String Following(int ind)
    {
        return alluser.get(index).getfollowingname(ind);
    }
    public static void liketweet()
    {
        Scanner in = new Scanner(System.in);
        boolean flag = false;
        System.out.println("----------------------------");
        System.out.println("Enter a tweetid to like : ");
        String tid = in.next() ;
        if(!isliked())
        {
            for(int i=0 ; i<alluser.size() ; i++)
            {
                for (int j=0 ; j<alluser.get(i).tweetsize() ; j++)
                {
                    if(tid.equals(alluser.get(i).getid(j)))
                    {
                        alluser.get(i).liketweet(j , alluser.get(index));
                        flag = true;
                    }
                }
            }
        }
        else if(isliked())
        {
            System.out.println("This tweet have been liked!");
            liketweet();
        }
        if(!flag)
        {
            System.out.println("Tweetid not found!");
            liketweet();
        }
        else
        {
            cls();
            actions();
        }
    }
    public static boolean isliked()
    {
        for(int i=0 ; i<alluser.size() ; i++)
        {
            for(int j=0 ; j<alluser.get(i).tweetsize() ; j++)
            {
                for(int k=0 ; k<alluser.get(i).getlikes(j).likenumber(); k++)
                {
                    if(alluser.get(index).getname().equals(alluser.get(i).getlikes(j).getlikeuser(k)))
                        return true;
                }

            }
        }
        return false;
    }
    public static ArrayList<String> Profile()
    {
        ArrayList<String> findtweets = new ArrayList<>();
        int andis = 0;
        Scanner in = new Scanner(System.in);
        System.out.println("Enter a username to check profile : ");
        String username = in.next() ;
        while(!isnameexisted(username))
        {
            System.out.println("-----------------------------");
            System.out.println("Username not found! try again.");
            username = in.next();
        }
        for(int i=0 ; i < alluser.size() ; i++)
        {
            if (username.equals(alluser.get(i).getname()))
                andis=i;
        }
        for(int i=0 ; i<alluser.get(andis).tweetsize() ; i++)
        {
            findtweets.add("user name : " + alluser.get(andis).getname() + " ---  tweet id : " + alluser.get(andis).getid(i) +
                    " ---  tweet text : " + alluser.get(andis).gettweet(i) + " ---  " + alluser.get(andis).likenum(i) + " likes" );
        }
        Collections.sort(findtweets);
        return findtweets;
    }
    public static ArrayList<String> Timeline()
    {
        ArrayList<String> sortedbytweet = new ArrayList<>();
        for (int i=0 ; i<alluser.get(index).tweetsize() ; i++)
        {
            String txt = "tweet id : " + alluser.get(index).getid(i) + " ---  tweet text : " +
                    alluser.get(index).gettweet(i) + " ---  written by : ( " + alluser.get(index).getname() +
                    " ) ---  " + alluser.get(index).likenum(i) + " likes ";
            sortedbytweet.add(txt);
        }
        for (int i=0 ; i<alluser.get(index).followingsize() ; i++)
        {
            for(int j=0 ; j<alluser.get(index).getfollowing(i).tweetsize() ; j++)
            {
                String txt = "tweet id : " + alluser.get(index).getfollowing(i).getid(j) + " ---  tweet text : " +
                        alluser.get(index).getfollowing(i).gettweet(j) + " ---  written by : ( " + alluser.get(index).getfollowing(i).getname() +
                        " ) ---  " + alluser.get(index).getfollowing(i).likenum(j) + " likes ";
                sortedbytweet.add(txt);
            }
        }
        Collections.sort(sortedbytweet);
        return sortedbytweet;
    }
}
