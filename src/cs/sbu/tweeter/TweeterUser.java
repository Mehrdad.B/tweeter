package cs.sbu.tweeter;

import java.util.ArrayList;

public class TweeterUser {
    private String username ;
    private String password ;
    private ArrayList<TweeterUser> followers;
    private ArrayList<TweeterUser> following;
    private ArrayList<Tweets> tweets ;
    public TweeterUser(String username , String password)
    {
        this.username = username;
        this.password = password;
        tweets = new ArrayList<>() ;
        followers = new ArrayList<>();
        following = new ArrayList<>();
    }
    public String getname()
    {
        return username;
    }
    public String getpass()
    {
        return password;
    }
    public void addtweet(Tweets tweet )
    {
        tweets.add(tweet);
    }
    public String gettweet(int ind)
    {
        return tweets.get(ind).getTweet();
    }
    public String getid(int ind)
    {
        return tweets.get(ind).getId();
    }
    public int tweetsize()
    {
        return tweets.size();
    }
    public void addfollowers(TweeterUser user)
    {
        followers.add(user);
    }
    public void deletefollowers(TweeterUser user)
    {
        followers.remove(user);
    }
    public void addfollowing(TweeterUser user)
    {
        following.add(user);
    }
    public void deletefollowing(TweeterUser user)
    {
        following.remove(user);
    }
    public String getfollowersname(int ind)
    {
        return followers.get(ind).getname();
    }
    public TweeterUser getfollowers(int ind)
    {
        return followers.get(ind);
    }
    public String getfollowingname(int ind)
    {
        return following.get(ind).getname();
    }
    public TweeterUser getfollowing(int ind)
    {
        return following.get(ind);
    }
    public int followerssize()
    {
        return followers.size();
    }
    public int followingsize()
    {
        return following.size();
    }
    public void liketweet(int ind , TweeterUser user)
    {
        tweets.get(ind).addlike(user);
    }
    public String likenum(int ind)
    {
        return Integer.toString(tweets.get(ind).likenumber());
    }
    public Tweets getlikes(int ind)
    {
        return tweets.get(ind);
    }
}
